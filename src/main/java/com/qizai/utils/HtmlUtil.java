package com.qizai.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Title: HtmlUtil.java
 * @Package com.qizai.utils
 * @Description: Html工具类
 * @Date 2013-7-22-上午11:00:20
 * @Author: qizai
 * @Version: V1.0.0
 */
public class HtmlUtil {
	// >
	public static final String GT = "&gt;";
	// <
	public static final String LT = "&lt;";
	// "
	public static final String QUOT = "&quot;";
	// &
	public static final String AMP = "&amp;";
	// 空格
	public static final String SPACE = "&nbsp;";
	// ©
	public static final String COPYRIGHT = "&copy;";
	// ®
	public static final String REG = "&reg;";
	// ™
	public static final String TM = "&trade;";
	// ¥
	public static final String RMB = "&yen;";

	/**
	 * 删除input字符串中的html格式
	 * 
	 * @param input
	 * @return
	 */
	public static String splitAndFilterString(String input) {
		if (input == null || "".equals(input.trim())) {
			return "";
		}
		// 去掉所有html元素,
		String str = input.replaceAll("\\&[a-zA-Z]{1,10};", "").replaceAll("<[^>]*>", "");
		str = str.replaceAll("[(/>)<]", "");
		return str;
	}

	/**
	 * 删除script标签
	 * 
	 * @param str
	 * @return
	 */
	public static String delScriptTag(String str) {
		String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>";
		Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
		Matcher m_script = p_script.matcher(str);
		str = m_script.replaceAll("");
		return str.trim();
	}

	/**
	 * 删除style标签
	 * 
	 * @param str
	 * @return
	 */
	public static String delStyleTag(String str) {
		String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>";
		Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
		Matcher m_style = p_style.matcher(str);
		str = m_style.replaceAll("");
		return str;
	}

	/**
	 * 删除HTML标签
	 * 
	 * @param str
	 * @return
	 */
	public static String delHTMLTag(String str) {
		String regEx_html = "<[^>]+>";
		Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
		Matcher m_html = p_html.matcher(str);
		str = m_html.replaceAll("");
		return str;
	}

	/**
	 * 删除所有标签
	 * 
	 * @param str
	 * @return
	 */
	public static String delAllTag(String str) {
		// 删script
		str = delScriptTag(str);
		// 删style
		str = delStyleTag(str);
		// 删HTML
		str = delHTMLTag(str);
		return str;
	}

	/**
	 * 清除标签,恢复HTML转义字符
	 * 
	 * @param str
	 * @return
	 */
	public static String clean(String str) {
		str = delAllTag(str);
		str = str.replaceAll(SPACE, " ");
		str = str.replaceAll(GT, ">");
		str = str.replaceAll(LT, "<");
		str = str.replaceAll(QUOT, "\"");
		str = str.replaceAll(AMP, "&");
		str = str.replaceAll(COPYRIGHT, "©");
		str = str.replaceAll(REG, "®");
		str = str.replaceAll(TM, "™");
		str = str.replaceAll(RMB, "¥");
		return str;
	}

	/**
	 * 过滤指定标签
	 * 
	 * @param str
	 * @param tag
	 *            指定标签
	 * @return String
	 */
	public static String fiterHtmlTag(String str, String tag) {
		String regxp = "<\\s*" + tag + "\\s+([^>]*)\\s*>";
		Pattern pattern = Pattern.compile(regxp);
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		boolean result1 = matcher.find();
		while (result1) {
			matcher.appendReplacement(sb, "");
			result1 = matcher.find();
		}
		matcher.appendTail(sb);
		return sb.toString();
	}
}
